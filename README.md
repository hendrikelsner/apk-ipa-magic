TiMagic
=======
A Simple GUI for automation of the Titanium CLI and ADB

To run the precompiled App you need Mac OS X 10.6+ (Because of 64-bit Python).

Get TiMagic
-----------
Get the App from /dist/timagic.app

OR Download /dist/timagic folder and run ´./timagic´ from within

OR Clone the repository and get all build requirements. Then fire up ´python timagic.py´.

OR Get the command-line version /timagic_console.py

Setup your Paths
----------------
Right click on timagic.app and choose "Show Contents". Edit Contents/MacOS/timagic_settings.xml.

Building from Source
--------------------
Requirements:
* Python 2.7 http://www.python.org/download/releases/2.7/
* Qt 4.7.4 http://download.qt-project.org/archive/qt/4.7/
* PySide 1.1.0 http://qt-project.org/wiki/Get-PySide
* Titanium CLI <= 3.2
* ADB 1.0.*
* PyInstaller 2.1 https://pypi.python.org/pypi/PyInstaller/2.1

1. After editing timagic.ui create a python module from it ´pyside-uic timagic.ui > ui_timagic.py´
2. Add your additional resources in timagic.spec
3. To package an App for iOS cd to project's root and run ´pyinstaller timagic.spec´
