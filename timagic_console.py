#!/usr/bin python
__author__      = "Hendrik Elsner"
__copyright__   = "Copyleft 2013"
__email__ = "321hendrik@gmail.com"
__version__ = "1.7"


import os
import subprocess
import xml.dom.minidom as dom
from multiprocessing import Process

def get_project_names():
	''' get project names '''
	project_names = []
	for elem in os.listdir(titanium_workspace_path):
		if elem[0].isalpha():
			project_names.append(elem)
	return project_names

def get_from_xml(xml_path, key):
	''' get the value for a key from an xml-file '''
	xml_tree = dom.parse(xml_path)
	value = 'none'
	for node in xml_tree.firstChild.childNodes:
		if node.nodeName == key: 
			value = node.firstChild.data.strip()
	return value

def remove_and_install(project_name, app_id, app_name, adb_path, titanium_workspace_path, device_id, sdk_version):
	''' install an apk to all connected adb devices and remove the old version if necessary '''
	print '...trying to uninstall old APK from ' + device_id
	uninstall_command = adb_path + ' -s ' + device_id + ' uninstall ' + app_id
	os.system(uninstall_command)
	print '...trying to install new APK to ' + device_id
	apk_path = titanium_workspace_path + project_name +'/build/android/bin/' + (app_name if float(sdk_version[0:3]) >= 3.2 else 'app') + '.apk'
	install_command = adb_path + ' -s ' + device_id + ' install ' + apk_path
	os.system(install_command)
	start_app_command = adb_path + ' -s ' + device_id + ' shell am start -n ' + app_id + '/' + app_id + '.' + app_name + 'Activity'
	os.system(start_app_command)

## read basic location from xml file
config_xml_path = 'timagic_settings.xml'
user_dir = get_from_xml(config_xml_path, 'user_dir')
titanium_workspace_path = get_from_xml(config_xml_path, 'titanium_workspace_path')
adb_path = get_from_xml(config_xml_path, 'adb_path')
# android
apk_output_path = get_from_xml(config_xml_path, 'apk_output_path')
keystore_path = get_from_xml(config_xml_path, 'keystore_path')
keystore_alias = get_from_xml(config_xml_path, 'keystore_alias')
keystore_pw = get_from_xml(config_xml_path, 'keystore_pw')
# iOS
ipa_output_path = get_from_xml(config_xml_path, 'ipa_output_path')
distribution_name = get_from_xml(config_xml_path, 'distribution_name')
pp_uuid = get_from_xml(config_xml_path, 'pp_uuid')	

# UI
answer = -1
notification = '\n'*2
projects = get_project_names()

# gui-loop
while True:
	# print the ui
	os.system('clear')
	print 'Done!\n'
	print '-'*20 + '= TiMagic '+__version__+' =' + '-'*20
	print ' '*2+'! Please setup your paths in timagic_settings.xml !\n'
	for i in range(len(projects)):
		print str(i)+' < '+projects[i]
	print 'x < exit the prompt'
	print notification
	input = raw_input('> ')
	project_num = ''
	ios_version = ''
	
	# handle user-input
	input_params = ''
	for i in str(input):
		if i == 'x':
			break
		elif i in ['0','1','2','3','4','5','6','7','8','9','.']:
			if len(input_params) > 0:
				ios_version += i
			else:
				project_num += i
		else: input_params += i
	if str(input)[0] == 'x':
		os.system('clear')
		break
	
	project_num = int(project_num)
	
	# check if project's number is valid
	if project_num < len(projects) and project_num >= 0 :
		# get build parameters
		os.system('clear')
		project_name = projects[project_num]
		tiapp_xml_path = titanium_workspace_path+project_name+'/tiapp.xml'
		sdk_version = get_from_xml(tiapp_xml_path, 'sdk-version')
		app_id = get_from_xml(tiapp_xml_path, 'id')
		app_name = get_from_xml(tiapp_xml_path, 'name').replace(' ', '')
		device_list = subprocess.check_output(adb_path + ' devices | grep device', shell=True).replace('\tdevice','').split('\n')[1:-1]
		ios_version = ios_version if (len(ios_version) > 0) else '7.0' # default to iOS-version 7.0
		
		if input_params == '':
			if len(device_list) > 0:
				# default build and install to all android devices
				print 'building APK'
				os.system('titanium build -b -s "' + sdk_version + '" -p "android" -d ' + titanium_workspace_path + project_name)
		
				# install the apk to all connected devices
				print 'installing ' + project_name + ' APK to all devices...'
				d = {}
				for i in device_list:
					d[i] = Process(target=remove_and_install, args=(project_name, app_id, app_name, adb_path, titanium_workspace_path, i, sdk_version))
					d[i].start()
				for i in device_list:
					d[i].join()
			else:
				notification = '\n --> Please connect a device first.\n'
				
		elif input_params == 'ipa':
			# build ad-hoc IPA for given iOS-version (e.g. for project number 1 and iOS-version 7.0 --> 1ipa7.0)
			build_ipa_command = 'titanium build -d '+ titanium_workspace_path + project_name +' -p ios -b -s ' + sdk_version + ' -R ' + distribution_name
			build_ipa_command += ' -I ' + ios_version + ' -P ' + pp_uuid + ' -O ' + ipa_output_path + ' -T dist-adhoc'
			os.system(build_ipa_command)
			
		elif input_params == 'apk':
			# build Play-Store APK (e.g. for project number 1 --> 1apk)
			build_apk_command = 'titanium build -b -s "' + sdk_version + '" -p android -d ' + titanium_workspace_path + project_name
			build_apk_command += ' -K ' + keystore_path + ' -L ' + keystore_alias + ' ' + ('--store-password' if float(sdk_version[0:3]) >= 3.2 else '--password') + ' ' + keystore_pw
			build_apk_command += ' -O ' + apk_output_path + ' -T dist-playstore'
			os.system(build_apk_command)
			
		#os.system('osascript -e \'tell app "System Events" to display dialog "APK successfully installed to all connected devices."\'') # popup on complete
	else:
		notification = '\n --> input out of range please enter valid numbers only\n'